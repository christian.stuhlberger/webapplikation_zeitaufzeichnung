/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean
@SessionScoped

public class GetAccessToken implements Serializable {

    public static String getToken() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        AccessToken access = (AccessToken) session.getAttribute("accessToken");

        String accessKey = access.getAccess();
        return accessKey;
    }
    
    public static String isValid() {
        String accessKey = GetAccessToken.getToken();
        String projects = "https://gitlab.com/api/v4/projects?membership=true&simple=true";
        try {
            

            
            URL projectURL = new URL(projects);

            HttpURLConnection con = (HttpURLConnection) projectURL.openConnection();
            con.setRequestProperty("PRIVATE-TOKEN", accessKey);
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            System.out.println("Sending 'GET' request to URL : " + projectURL);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            
            
           if(responseCode == 200)
           {
               return "listOfProjectsPage";
           }
           
            
            
               
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
