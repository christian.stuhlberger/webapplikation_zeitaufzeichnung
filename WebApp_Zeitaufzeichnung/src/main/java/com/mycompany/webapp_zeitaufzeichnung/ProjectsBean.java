/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean
@SessionScoped
@Named
public class ProjectsBean implements Serializable {

    //gibt alle Projekte des AccessToken Benutzers zurück
    public ArrayList allProjects = new ArrayList();

    public Project selectedProject;

    public String txt1;

    public Project getSelectedProject() {
        return selectedProject;
    }

    public List<String> autocompleteItems = new ArrayList<>();

    ArrayList filteredProjectList;
    ArrayList restoreAllProjects;

    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    @PostConstruct
    public void callAllProjects() {

        String accessKey = GetAccessToken.getToken();
        String projects = "https://gitlab.com/api/v4/projects?membership=true&simple=true";
        try {

            URL projectURL = new URL(projects);

            HttpURLConnection con = (HttpURLConnection) projectURL.openConnection();
            con.setRequestProperty("PRIVATE-TOKEN", accessKey);
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            System.out.println("Sending 'GET' request to URL : " + projectURL);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //print in String
            System.out.println(response.toString());
            //Read JSON response and print
            JSONArray myProjectRespone = new JSONArray(response.toString());

            for (int i = 0; i < myProjectRespone.length(); i++) {
                JSONObject nameOfProject = myProjectRespone.getJSONObject(i);
                int projectId = nameOfProject.getInt("id");
                StudentsBean student = new StudentsBean();
                IssuesBean issue = new IssuesBean();
                ArrayList arr = student.callEveryone(projectId);
                autocompleteItems.add(nameOfProject.getString("name"));
                allProjects.add(new Project(nameOfProject.getString("name"), arr, issue.callEveryone(projectId, arr), issue.callHashMap(), "totalEstimate: " + issue.callEstimateTime() + "h" + ", " + "totalSpend: " + issue.callSpendTime() + "h"));
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | JSONException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        restoreAllProjects = new ArrayList(allProjects);
    }

    public List<String> completeText(String query) {

        if (query.trim().equals('.')) {
            allProjects = new ArrayList(restoreAllProjects);
        } else {

            List<String> results = new ArrayList<>();
            int k = -1;
            allProjects = new ArrayList(restoreAllProjects);
            for (int i = 0; i < autocompleteItems.size(); i++) {

                k++;

                Project p = (Project) restoreAllProjects.get(i);
                String current = p.getName();
                String current2 = p.getStudents();
                if (current.toLowerCase().contains(query.trim().toLowerCase()) || current2.toLowerCase().contains(query.trim().toLowerCase())) {
                    results.add(current);
                } else {
                    allProjects.remove(k);
                    k--;
                }

            }

            return results;
        }
        return null;
    }

    public void filterProjectList() {
        // allProjects = filteredProjectList;
        allProjects = new ArrayList(restoreAllProjects);
    }

    public ArrayList getAllProjects() {
        return allProjects;
    }

    public void setAllProjects(ArrayList allProjects) {
        this.allProjects = allProjects;
    }

    public String getToDetailPage(Project p) {
        setSelectedProject(p);
        return "detailpage";
    }

    public List<String> getAutocompleteItems() {
        return autocompleteItems;
    }

    public void setAutocompleteItems(ArrayList<String> autocompleteItems) {
        this.autocompleteItems = autocompleteItems;
    }

    public String getTxt1() {
        return txt1;
    }

    public void setTxt1(String txt1) {
        this.txt1 = txt1;
    }

    public void download() throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        ec.responseReset();
        ec.setResponseContentType("text/csv");
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + selectedProject.getName() + ".csv" + "\"");

        OutputStream output = ec.getResponseOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(output));
        bw.write("Projekttitel;"+selectedProject.getName() + "\n");
        bw.write("Projektmitglieder;"+selectedProject.getStudents() + "\n");
        bw.newLine();
        bw.write("Sprints mit Issues" + "\n");
        for (Object o : selectedProject.getOrderedIssues()) {
            SprintWrapper sprint = (SprintWrapper) o;
            sprint.sortIssues();
            bw.write(sprint.getName()+"\n");
            bw.write("Issues \n");
            for (Issue i : sprint.getIssues()) {
                bw.write("Zugeteilt an:;" + i.assignee + "\n");
                bw.write("Title;" + i.title + "\n");
                bw.write("Spend;" + i.spend + "\n");
                bw.write("Estimated;" + i.estimate + "\n");
                bw.write("State;" + i.state + "\n");
                bw.newLine();
            }
            bw.newLine();
        }

        bw.flush();
        bw.close();

        fc.responseComplete();
    }
}
