/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author user
 */
public class Project {

    private String name;
    private String studentsString;
    private ArrayList<Issue> issues;
    private ArrayList spendTime;
    private String estimateSpendTotal;
    private ArrayList<User> users;
    private ArrayList<PieChartModel> teamCharts;

    public Project() {
    }

    public Project(String name, ArrayList studentsArr, ArrayList issues, ArrayList spendTime, String estimateSpendTotal) {
        this.name = name;
        this.studentsString = "";
        this.users = new ArrayList<>();
        for (int j = 0; studentsArr.size() > j; j++) {
            this.studentsString += studentsArr.get(j) + ",";
            this.users.add(new User(studentsArr.get(j).toString().trim()));
        }
        this.studentsString = this.studentsString.substring(0, studentsString.length() - 1);
        this.issues = issues;
        this.spendTime = spendTime;
        this.estimateSpendTotal = estimateSpendTotal;
        this.teamCharts = new ArrayList<PieChartModel>();
        
        initUsersDataStatistics();
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudents() {
        return studentsString;
    }

    public void setStudents(String students) {
        this.studentsString = students;
    }

    public ArrayList getIssues() {
        return issues;
    }

    public void setIssues(ArrayList issues) {
        this.issues = issues;
    }

    public ArrayList getSpendTime() {
        return spendTime;
    }

    public void setSpendTime(ArrayList spendTime) {
        this.spendTime = spendTime;
    }

    public String getEstimateSpendTotal() {
        return estimateSpendTotal;
    }

    public void setEstimateSpendTotal(String estimateSpendTotal) {
        this.estimateSpendTotal = estimateSpendTotal;
    }

    public ArrayList<PieChartModel> getTeamCharts() {
        return teamCharts;
    }

    public ArrayList<SprintWrapper> getOrderedIssues() {
        HashSet<String> localSprints = new HashSet<String>();
        for (int i = 0; i < issues.size(); i++) {
            ArrayList<String> labels = ((Issue) issues.get(i)).labels;
            for (int x = 0; x < labels.size(); x++) {
                localSprints.add(labels.get(x));
            }
        }

        ArrayList<SprintWrapper> ordered = new ArrayList<SprintWrapper>();
        for (Object s : localSprints.toArray()) {
            ArrayList a = new ArrayList();
            SprintWrapper sprint = new SprintWrapper();
            sprint.setName(s.toString());
            ordered.add(sprint);
        }

        for (int i = 0; i < issues.size(); i++) {
            int counter = 0;
            for (SprintWrapper s : ordered) {
                Issue issue = ((Issue) issues.get(i));
                if (issue.labels.contains(s.getName())) {
                    s.issues.add(issue);
                }
            }
        }

        boolean modified = true;
        while (modified) {
            modified = false;
            int prevNumSprint = -1;
            for (int i = 0; i < ordered.size(); i++) {
                if (prevNumSprint > ordered.get(i).getSprintNum()) {
                    SprintWrapper sprint = ordered.get(i);
                    ordered.set(i, ordered.get(i - 1));
                    ordered.set(i - 1, sprint);
                    modified = true;
                }
                prevNumSprint = ordered.get(i).getSprintNum();
            }
        }

        for (SprintWrapper s : ordered) {
            s.sortIssues();
        }

        return ordered;
    }

    @Override
    public String toString() {
        return "Project{" + "name=" + name + ", students=" + studentsString + ", issues=" + issues + ", spendTime=" + spendTime + ", estimateSpendTotal=" + estimateSpendTotal + '}';
    }

    private void initUsersDataStatistics() {
        boolean a = true;
        boolean b = true;
        boolean c = true;
        for (User u : users) {
            for (Issue i : issues) {
                a = i.assignee.equals(u.getNameOfUser());
                b = !i.isEstimatedNotSet();
                c = !i.isSpendNotSet();

                if (a) {
                    if (b) {
                        if (c) {
                            u.incrementTotalIssues();
                            u.addTotalEstimated(i.getEstimateFloat());
                            u.addTotalSpend(i.getSpendFloat());
                            u.addIssue(i);

                        }
                    }
                }
            }
        }

        for (User u : users) {
            if(u.isWorkerOfProjekt()){  
                u.initCharts();   
            }
        }
        
        initChartsTeam();
    }

    private void initChartsTeam() {
        PieChartModel pieModel = new PieChartModel();
        for (User u : users) {
            if(u.getTotalSpend() == 0) continue;
            pieModel.set(u.getNameOfUser(), u.getTotalSpend());
           // System.out.println(u.getNameOfUser()+"  "+u.getTotalSpend());
        }
        pieModel.setTitle("Arbeitsverteilung mit gearbeiter Zeit");
        pieModel.setLegendPosition("e");
        pieModel.setFill(false);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(200);
        pieModel.setShadow(false);
        teamCharts.add(pieModel);

        pieModel = new PieChartModel();
        for (User u : users) {
            if(u.getTotalIssuesAssigned()==0) continue;
            pieModel.set(u.getNameOfUser(), u.getTotalIssuesAssigned());
            System.out.println(u.getNameOfUser()+"  "+u.getTotalIssuesAssigned());
        }
        pieModel.setTitle("Arbeitsverteilung mit zugeteilten Issues");
        pieModel.setLegendPosition("e");
        pieModel.setFill(false);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(200);
        pieModel.setShadow(false);
        
        teamCharts.add(pieModel);
    }

}
