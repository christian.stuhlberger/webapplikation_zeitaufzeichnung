/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Issue {
    String title;
    String estimate;
    String spend;
    String assignee;
    ArrayList <String> labels;
    String state;

    public Issue() {
        labels=new ArrayList<String>();
    }

    public Issue(String title, String estimate, String spend, ArrayList<String> labelsJSON) {
        this.title = title;
        this.estimate = estimate;
        this.spend = spend;
        this.assignee="Nicht zugewiesen";
        this.labels = labelsJSON;
    }

    public Issue(String title, String estimate, String spend, String assignee, ArrayList<String> labelsJSON) {
        this.title = title;
        this.estimate = estimate;
        this.spend = spend;
        this.assignee = assignee;
        this.labels = labelsJSON;
    }

    public Issue(String title, String estimate, String spend, String assignee, ArrayList<String> labels, String state) {
        this.title = title;
        this.estimate = estimate;
        this.spend = spend;
        this.assignee = assignee;
        this.labels = labels;
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public String getSpend() {
        return spend;
    }

    public void setSpend(String spend) {
        this.spend = spend;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<String> labels) {
        this.labels = labels;
    }
    
    public boolean isInMoreThanOneSprint(){
        int num=-1;
        for(String s:labels){
            int numLocal=MethodsNeedForCasting.getNumOfSprintLabel(s);
            if(numLocal != -1){
                if(num == -1){
                    num=numLocal;
                }else{
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean isEstimatedNotSet(){
       return !estimate.contains(".");
    }
    
    public boolean isSpendNotSet(){
        return !estimate.contains(".");
    }
    
    public float getSpendFloat(){
        float s=0;
        try{
            s=Float.parseFloat(this.spend.substring(0,this.spend.length()-1));
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return s;
    }
    
     public float getEstimateFloat(){
        float est=0;
        try{
            est=Float.parseFloat(this.estimate.substring(0,estimate.length()-1));
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return est;
    }
    

    @Override
    public String toString() {
        return "Issue{" + "title=" + title + ", estimate=" + estimate + ", spend=" + spend + ", assignee=" + assignee + '}';
    }
    
    

   
    
    
   
    
    
    
}
