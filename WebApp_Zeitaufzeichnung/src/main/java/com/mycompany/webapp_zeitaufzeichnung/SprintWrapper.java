/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.util.ArrayList;

/**
 *
 * @author mdopp
 */
public class SprintWrapper {

    private int sprintNum;
    private String name;
    ArrayList<Issue> issues;

    public SprintWrapper() {
        name = "";
        sprintNum = -1;
        issues = new ArrayList<Issue>();
    }

    public int getSprintNum() {
        return sprintNum;
    }

    public void setSprintNum(int sprintNum) {
        this.sprintNum = sprintNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.sprintNum=MethodsNeedForCasting.getNumOfSprintLabel(name);
    }

    public ArrayList<Issue> getIssues() {
        return issues;
    }

    public void setIssues(ArrayList<Issue> issues) {
        this.issues = issues;
    }

    public void sortIssues() {
        for (int i = 0; i < issues.size(); i++) {
            Issue issue = ((Issue) issues.get(i));
            if (issue.state.equals("closed")) {
                issues.remove(i);
                issues.add(issue);
            }
        }
    }

}
