/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
public class IssuesBean {

    public ArrayList issueList = new ArrayList();

    AccessToken access;
    public HashMap hashMapUserTimeSpend = new HashMap();
    double spendCounter;
    double estimateCounter;

    public ArrayList arrKeyVal = new ArrayList();

    public ArrayList getArrKeyVal() {
        return arrKeyVal;
    }

    public void setArrKeyVal(ArrayList arrKeyVal) {
        this.arrKeyVal = arrKeyVal;
    }

    public ArrayList getIssueList() {
        return issueList;
    }

    public void setIssueList(ArrayList issueList) {
        this.issueList = issueList;
    }

    public ArrayList callEveryone(int projectId, ArrayList students) {
        spendCounter = 0;
        estimateCounter = 0;
        String accessKey = GetAccessToken.getToken();

        for (int i = 0; i < students.size(); i++) {
            hashMapUserTimeSpend.put(students.get(i).toString(), (double) 0);

        }

        String url = "https://gitlab.com/api/v4/projects/" + projectId + "/issues";
        // String key = "QfCSL6tkw1s9AeVtQJ1A";
        URL obj;
        try {
            obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("PRIVATE-TOKEN", accessKey);
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            System.out.println("Sending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //print in String
            System.out.println(response.toString());
            //Read JSON response and print
            JSONArray myResponse = new JSONArray(response.toString());

            for (int i = 0; i < myResponse.length(); i++) {
                JSONObject titleName = myResponse.getJSONObject(i);
                JSONObject checkTime = titleName.getJSONObject("time_stats");
                String state = titleName.getString("state");
                JSONArray nameofIssuePerson = titleName.getJSONArray("assignees");
                JSONArray labelsJSON = titleName.getJSONArray("labels");
                ArrayList labels = new ArrayList();
                for (int x = 0; x < labelsJSON.length(); x++) {
                    labels.add(labelsJSON.get(x));
                }

                if (labels.size() == 0) {
                    if (state.equals("opened")) {
                        labels.add("Open");
                    }
                }

                if (nameofIssuePerson.length() < 1) {
                    Issue issue = new Issue(titleName.getString("title"), "Keine Zeit geplant", "Kein Zeit gebucht", labels);
                    issue.state = state;
                    issueList.add(issue);
                } else {

                    JSONObject assigneeOfIssues = nameofIssuePerson.getJSONObject(0);

                    String s = " " + assigneeOfIssues.getString("username");
                    boolean valuee = hashMapUserTimeSpend.containsKey(s);
                    int spendTimeInSec = 0;
                    int estimateTimeInSec = 0;
                    if (valuee) {
                        double value = (double) hashMapUserTimeSpend.get(s);
                        spendTimeInSec = checkTime.getInt("total_time_spent");
                        estimateTimeInSec = checkTime.getInt("time_estimate");

                        if (spendTimeInSec != 0) {
                            double timeInHour = spendTimeInSec / 3600;
                            double totalD = value + timeInHour;
                            spendCounter = spendCounter + timeInHour;
                            hashMapUserTimeSpend.put(s, totalD);
                        }

                        if (estimateTimeInSec != 0) {
                            double timeInHour = estimateTimeInSec / 3600;
                            estimateCounter = estimateCounter + timeInHour;
                        }
                    }

                    issueList.add(new Issue(titleName.getString("title"), (double) estimateTimeInSec / 3600 + "h", (double) spendTimeInSec / 3600 + "h", assigneeOfIssues.getString("username"), labels, state));
                }
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return issueList;
    }

    public ArrayList callHashMap() {
        ArrayList keyList = new ArrayList(hashMapUserTimeSpend.keySet());

        for (int i = 0; i < keyList.size(); i++) {
            String s = (String) keyList.get(i);
            double d = (double) hashMapUserTimeSpend.get(keyList.get(i));

            String keyValue = s + ": " + d + "h";
            arrKeyVal.add(keyValue);

        }
        return arrKeyVal;

    }

    public double callSpendTime() {
        return spendCounter;
    }

    public double callEstimateTime() {
        return estimateCounter;
    }

}
