/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.util.ArrayList;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author mdopp
 */
public class User {

    private String nameOfUser;
    private int totalIssuesAssigned;
    private float totalSpend;
    private float totalEstimated;
    private ArrayList<PieChartModel> chartModels;
    private ArrayList<Issue> issues;

    public User() {
        chartModels = new ArrayList<>();
        issues = new ArrayList<>();
    }

    public User(String nameOfUser) {
        this();
        this.nameOfUser = nameOfUser;
    }

    public String getNameOfUser() {
        return nameOfUser;
    }

    public void setNameOfUser(String nameOfUser) {
        this.nameOfUser = nameOfUser;
    }

    public int getTotalIssuesAssigned() {
        return totalIssuesAssigned;
    }

    public void setTotalIssuesAssigned(int totalIssuesAssigned) {
        this.totalIssuesAssigned = totalIssuesAssigned;
    }

    public float getTotalSpend() {
        return totalSpend;
    }

    public void setTotalSpend(float totalSpend) {
        this.totalSpend = totalSpend;
    }

    public float getTotalEstimated() {
        return totalEstimated;
    }

    public void setTotalEstimated(float totalEstimated) {
        this.totalEstimated = totalEstimated;
    }

    public void addTotalEstimated(float estimated) {
        this.totalEstimated += estimated;
    }

    public void addTotalSpend(float estimated) {
        this.totalSpend += estimated;
    }

    public void incrementTotalIssues() {
        this.totalIssuesAssigned++;
    }

    public void addIssue(Issue issue) {
        this.issues.add(issue);
    }

    public ArrayList<PieChartModel> getChartModels() {
        return chartModels;
    }

    public void setChartModels(ArrayList<PieChartModel> chartModels) {
        this.chartModels = chartModels;
    }

    public ArrayList<Issue> getIssues() {
        return issues;
    }

    public void setIssues(ArrayList<Issue> issues) {
        this.issues = issues;
    }

    public void initCharts() {
        PieChartModel pieModel = new PieChartModel();
        pieModel.set("Total Estimated", totalEstimated);
        pieModel.set("Total Spend", totalSpend);
        pieModel.setTitle("Geplante VS Verwendete Zeit insgesamt");
        pieModel.setLegendPosition("e");
        pieModel.setFill(false);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(200);
        pieModel.setShadow(false);

        chartModels.add(pieModel);

        pieModel = new PieChartModel();
        for (Issue i : issues) {
            if (i.title.length() - 1 < 30) {
                pieModel.set(i.title, i.getSpendFloat());
            }else{
                pieModel.set(i.title.substring(0,28)+"..", i.getSpendFloat());
            }
        }
        pieModel.setTitle("Issues und verwendete Zeit");
        pieModel.setLegendPosition("e");
        pieModel.setFill(false);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(200);
        pieModel.setShadow(false);

        chartModels.add(pieModel);

        pieModel = new PieChartModel();
        for (Issue i : issues) {
             if (i.title.length() - 1 < 30) {
                pieModel.set(i.title, i.getEstimateFloat());
            }else{
                 pieModel.set(i.title.substring(0,28)+"..", i.getEstimateFloat());
            }
        }
        pieModel.setTitle("Issues und geplante Zeit");
        pieModel.setLegendPosition("e");
        pieModel.setFill(false);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(200);
        pieModel.setShadow(false);

        chartModels.add(pieModel);
    }

    @Override
    public String toString() {
        return "User{" + "nameOfUser=" + nameOfUser + ", totalIssuesAssigned=" + totalIssuesAssigned + ", totalSpend=" + totalSpend + ", totalEstimated=" + totalEstimated + '}';
    }

    boolean isWorkerOfProjekt() {
        return issues.size() > 0;
    }

}
