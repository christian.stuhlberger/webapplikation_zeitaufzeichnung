/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

/**
 *
 * @author mdopp
 */
public class MethodsNeedForCasting {

    public static int getNumOfSprintLabel(String name) {
        int sprintNum = -1;
        char[] arr = name.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            try {
                if (sprintNum == -1) {
                    sprintNum = Integer.parseInt("" + arr[i]);
                } else {
                    sprintNum = sprintNum * 10 + Integer.parseInt("" + arr[i]);
                }
            } catch (NumberFormatException e) {

            }
        }
        return sprintNum;
    }
}
