/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webapp_zeitaufzeichnung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean
@Named
public class StudentsBean implements Serializable {

    //gibt alle Usernamen eines Projekts zurück
    public ArrayList studentsList = new ArrayList();

    AccessToken access;

    
    public  ArrayList callEveryone(int projectId) {
        String accessKey = GetAccessToken.getToken();

        String url = "https://gitlab.com/api/v4/projects/"+projectId+"/members";
        // String key = "QfCSL6tkw1s9AeVtQJ1A";
        URL obj;
        try {
            obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("PRIVATE-TOKEN", accessKey);
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            System.out.println("Sending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //print in String
            System.out.println(response.toString());
            //Read JSON response and print
            JSONArray myResponse = new JSONArray(response.toString());

            for (int i = 0; i < myResponse.length(); i++) {
                JSONObject username = myResponse.getJSONObject(i);
                System.out.println("Username- " + username.getString("username"));
                studentsList.add(new Student(username.getString("username")));

            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(StudentsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return studentsList;
    }

    public ArrayList getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(ArrayList studentsList) {
        this.studentsList = studentsList;
    }

}
